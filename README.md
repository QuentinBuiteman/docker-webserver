WORK IN PROGRESS!

# Docker Webserver
A Docker setup to easily deploy a local webserver with Apache or Nginx and your desired PHP version.

## Setup
Add this project to your git repository as a submodule.
```
git submodule add https://bitbucket.com/QuentinBuiteman/docker-webserver.git projectname-webserver
```
Navigate to the created folder to create your .env file.
```
cd projectname-webserver
cp .env.example .env
```
Change the env file to your liking. You can set the PHP version and the MySQL user, password and database name.

### Apache
If you would like to use Apache, copy the default config file and change the DocumentRoot and ServerName to your liking.
```
cd apache/sites-enabled
cp 000-default.conf.example 000-default.conf
```

### Nginx
If you would like to use Nginx, copy the default config file and change the server_name and root to your liking.
```
cd nginx/conf.d
cp default.conf.example default.conf
```

## Startup
Navigate into the submodule directory.
```
cd projectname-webserver
```

### Apache
```
docker-compose up -d apache mysql
```

### Nginx
```
docker-compose up -d nginx mysql
```

## Supported PHP versions
* 5.5
* 5.6
* 7.0
* 7.1
